//
//  MovieSummaryParsingTests.swift
//  ms-tech-testTests
//
//  Created by acb on 11/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import XCTest
@testable import ms_tech_test

class MovieSummaryParsingTests: XCTestCase {
    
    func testParseMovieSummary() {
        let jsonData = "{ \"vote_count\": 2868, \"id\": 181808, \"video\": false, \"vote_average\": 7.2, \"title\": \"Star Wars: The Last Jedi\", \"popularity\": 538.442545, \"poster_path\": \"/xGWVjewoXnJhvxKW619cMzppJDQ.jpg\", \"original_language\": \"en\", \"original_title\": \"Star Wars: The Last Jedi\", \"genre_ids\": [ 28, 12, 14, 878 ], \"backdrop_path\": \"/5Iw7zQTHVRBOYpA0V6z0yypOPZh.jpg\", \"adult\": false, \"overview\": \"Rey develops her newly discovered abilities with the guidance of Luke Skywalker, who is unsettled by the strength of her powers. Meanwhile, the Resistance prepares to do battle with the First Order.\", \"release_date\": \"2017-12-13\" }".data(using: .utf8)!
        
        let decoder = JSONDecoder()
        
        let parsed: MovieSummary = try! decoder.decode(MovieSummary.self, from: jsonData)
        XCTAssertEqual(parsed.id, 181808)
        XCTAssertEqual(parsed.title, "Star Wars: The Last Jedi")
        XCTAssertEqual(parsed.overview, "Rey develops her newly discovered abilities with the guidance of Luke Skywalker, who is unsettled by the strength of her powers. Meanwhile, the Resistance prepares to do battle with the First Order.")
        XCTAssertEqual(parsed.posterPath, "/xGWVjewoXnJhvxKW619cMzppJDQ.jpg")
        XCTAssertEqual(parsed.backdropPath, "/5Iw7zQTHVRBOYpA0V6z0yypOPZh.jpg")
        XCTAssertEqual(parsed.popularity, 538.442545)
        XCTAssertEqual(parsed.releaseDate, "2017-12-13")
        XCTAssertEqual(parsed.votes?.average, 7.2)
        XCTAssertEqual(parsed.votes?.count, 2868)
        XCTAssertEqual(parsed.originalLanguage, "en")
        XCTAssertEqual(parsed.isAdult, false)
    }
    
}
