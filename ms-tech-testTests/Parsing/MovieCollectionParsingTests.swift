//
//  MovieCollectionParsingTests.swift
//  ms-tech-testTests
//
//  Created by acb on 11/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import XCTest
@testable import ms_tech_test

class MovieCollectionParsingTests: XCTestCase {
    
    func testParseMovieCollection() {
        let jsonData = "{\"id\": 10, \"name\": \"Star Wars Collection\", \"overview\": \"An epic space opera theatrical film series that depicts the adventures of various characters \\\"a long time ago in a galaxy far, far away\\\".\", \"poster_path\": \"/ghd5zOQnDaDW1mxO7R5fXXpZMu.jpg\", \"backdrop_path\": \"/d8duYyyC9J5T825Hg7grmaabfxQ.jpg\", \"parts\": [ { \"adult\": false, \"backdrop_path\": \"/c4zJK1mowcps3wvdrm31knxhur2.jpg\", \"genre_ids\": [ 12, 28, 878 ], \"id\": 11, \"original_language\": \"en\", \"original_title\": \"Star Wars\", \"overview\": \"Princess Leia is captured and held hostage by the evil Imperial forces in their effort to take over the galactic Empire. Venturesome Luke Skywalker and dashing captain Han Solo team together with the loveable robot duo R2-D2 and C-3PO to rescue the beautiful princess and restore peace and justice in the Empire.\", \"release_date\": \"1977-05-25\", \"poster_path\": \"/btTdmkgIvOi0FFip1sPuZI2oQG6.jpg\", \"popularity\": 54.342334, \"title\": \"Star Wars\", \"video\": false, \"vote_average\": 8.1, \"vote_count\": 7760 }, { \"adult\": false, \"backdrop_path\": \"/amYkOxCwHiVTFKendcIW0rSrRlU.jpg\", \"genre_ids\": [ 12, 28, 878 ], \"id\": 1891, \"original_language\": \"en\", \"original_title\": \"The Empire Strikes Back\", \"overview\": \"The epic saga continues as Luke Skywalker, in hopes of defeating the evil Galactic Empire, learns the ways of the Jedi from aging master Yoda. But Darth Vader is more determined than ever to capture Luke. Meanwhile, rebel leader Princess Leia, cocky Han Solo, Chewbacca, and droids C-3PO and R2-D2 are thrown into various stages of capture, betrayal and despair.\", \"release_date\": \"1980-05-20\", \"poster_path\": \"/6u1fYtxG5eqjhtCPDx04pJphQRW.jpg\", \"popularity\": 31.042075, \"title\": \"The Empire Strikes Back\", \"video\": false, \"vote_average\": 8.2, \"vote_count\": 6787 }, { \"adult\": false, \"backdrop_path\": \"/koE7aMeR2ATivI18mCbscLsI0Nm.jpg\", \"genre_ids\": [ 12, 28, 878 ], \"id\": 1892, \"original_language\": \"en\", \"original_title\": \"Return of the Jedi\", \"overview\": \"As Rebel leaders map their strategy for an all-out attack on the Emperor's newer, bigger Death Star. Han Solo remains frozen in the cavernous desert fortress of Jabba the Hutt, the most loathsome outlaw in the universe, who is also keeping Princess Leia as a slave girl. Now a master of the Force, Luke Skywalker rescues his friends, but he cannot become a true Jedi Knight until he wages his own crucial battle against Darth Vader, who has sworn to win Luke over to the dark side of the Force.\", \"release_date\": \"1983-05-23\", \"poster_path\": \"/jx5p0aHlbPXqe3AH9G15NvmWaqQ.jpg\", \"popularity\": 35.711479, \"title\": \"Return of the Jedi\", \"video\": false, \"vote_average\": 7.9, \"vote_count\": 5437 } ] }".data(using: .utf8)!
        
        let decoder = JSONDecoder()
        
        let parsed: MovieCollection = try! decoder.decode(MovieCollection.self, from: jsonData)
        XCTAssertEqual(parsed.id, 10)
        XCTAssertEqual(parsed.name, "Star Wars Collection")
        XCTAssertEqual(parsed.overview, "An epic space opera theatrical film series that depicts the adventures of various characters \"a long time ago in a galaxy far, far away\".")
        XCTAssertEqual(parsed.backdropPath, "/d8duYyyC9J5T825Hg7grmaabfxQ.jpg")
        XCTAssertEqual(parsed.posterPath, "/ghd5zOQnDaDW1mxO7R5fXXpZMu.jpg")
        XCTAssertEqual(parsed.parts.count, 3)
        XCTAssertEqual(parsed.parts[0].id, 11)
        XCTAssertEqual(parsed.parts[1].id, 1891)
        XCTAssertEqual(parsed.parts[2].id, 1892)
    }
    
}
