//
//  MovieDetailParsingTests.swift
//  ms-tech-testTests
//
//  Created by acb on 11/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import XCTest
@testable import ms_tech_test

class MovieDetailParsingTests: XCTestCase {
 
    func testParseMovieDetail() {
        let jsonData = "{ \"adult\": false, \"backdrop_path\": \"/5Iw7zQTHVRBOYpA0V6z0yypOPZh.jpg\", \"belongs_to_collection\": { \"id\": 10, \"name\": \"Star Wars Collection\", \"poster_path\": \"/ghd5zOQnDaDW1mxO7R5fXXpZMu.jpg\", \"backdrop_path\": \"/d8duYyyC9J5T825Hg7grmaabfxQ.jpg\" }, \"budget\": 200000000, \"genres\": [ { \"id\": 28, \"name\": \"Action\" }, { \"id\": 12, \"name\": \"Adventure\" }, { \"id\": 14, \"name\": \"Fantasy\" }, { \"id\": 878, \"name\": \"Science Fiction\" } ], \"homepage\": \"http://www.starwars.com\", \"id\": 181808, \"imdb_id\": \"tt2527336\", \"original_language\": \"en\", \"original_title\": \"Star Wars: The Last Jedi\", \"overview\": \"Rey develops her newly discovered abilities with the guidance of Luke Skywalker, who is unsettled by the strength of her powers. Meanwhile, the Resistance prepares to do battle with the First Order.\", \"popularity\": 537.442545, \"poster_path\": \"/xGWVjewoXnJhvxKW619cMzppJDQ.jpg\", \"production_companies\": [ { \"name\": \"Lucasfilm\", \"id\": 1 }, { \"name\": \"Walt Disney Pictures\", \"id\": 2 } ], \"production_countries\": [ { \"iso_3166_1\": \"US\", \"name\": \"United States of America\" } ], \"release_date\": \"2017-12-13\", \"revenue\": 1206728146, \"runtime\": 152, \"spoken_languages\": [ { \"iso_639_1\": \"en\", \"name\": \"English\" } ], \"status\": \"Released\", \"tagline\": \"The Saga Continues\", \"title\": \"Star Wars: The Last Jedi\", \"video\": false, \"vote_average\": 7.2, \"vote_count\": 2881 }".data(using: .utf8)!
        
        let decoder = JSONDecoder()
        
        let parsed: MovieDetail = try! decoder.decode(MovieDetail.self, from: jsonData)
        XCTAssertEqual(parsed.id, 181808)
        XCTAssertEqual(parsed.title, "Star Wars: The Last Jedi")
        XCTAssertEqual(parsed.tagline, "The Saga Continues")
        XCTAssertEqual(parsed.overview, "Rey develops her newly discovered abilities with the guidance of Luke Skywalker, who is unsettled by the strength of her powers. Meanwhile, the Resistance prepares to do battle with the First Order.")
        XCTAssertEqual(parsed.posterPath, "/xGWVjewoXnJhvxKW619cMzppJDQ.jpg")
        XCTAssertEqual(parsed.backdropPath, "/5Iw7zQTHVRBOYpA0V6z0yypOPZh.jpg")
        XCTAssertEqual(parsed.releaseDate, "2017-12-13")
        XCTAssertEqual(parsed.popularity, 537.442545)
        XCTAssertEqual(parsed.votes?.average, 7.2)
        XCTAssertEqual(parsed.votes?.count, 2881)
        XCTAssertEqual(parsed.originalLanguage, "en")
        XCTAssertEqual(parsed.budget, 200000000)
        XCTAssertEqual(parsed.revenue, 1206728146)
        XCTAssertEqual(parsed.runtime, Measurement(value: 152, unit: .minutes))
        XCTAssertEqual(parsed.isAdult, false)
        XCTAssertEqual(parsed.collection, NamedGrouping(id: 10, name: "Star Wars Collection", posterPath: "/ghd5zOQnDaDW1mxO7R5fXXpZMu.jpg", backdropPath: "/d8duYyyC9J5T825Hg7grmaabfxQ.jpg"))
        XCTAssertEqual(parsed.genres!, [
            NamedGrouping(id: 28, name: "Action"),
            NamedGrouping(id: 12, name: "Adventure"),
            NamedGrouping(id: 14, name: "Fantasy"),
            NamedGrouping(id: 878, name: "Science Fiction")
        ])
    }

    
}
