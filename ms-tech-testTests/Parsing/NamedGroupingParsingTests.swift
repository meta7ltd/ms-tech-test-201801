//
//  NamedGroupingParsingTests.swift
//  ms-tech-testTests
//
//  Created by acb on 11/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import XCTest
@testable import ms_tech_test

class NamedGroupingParsingTests: XCTestCase {
    
    func testParseNamedsGroupingWithImages() {
        let jsonData = "{ \"id\": 123, \"name\": \"Name goes here\", \"poster_path\":\"/foobar123.jpg\", \"backdrop_path\": \"/qwertyasdf.jpg\" }".data(using: .utf8)!
        let decoder = JSONDecoder()
        
        let parsed: NamedGrouping = try! decoder.decode(NamedGrouping.self, from: jsonData)

        XCTAssertEqual(parsed, NamedGrouping(id: 123, name: "Name goes here", posterPath:"/foobar123.jpg", backdropPath: "/qwertyasdf.jpg"))
    }
    
    func testParseNamedGroupingWithoutImages() {
        let jsonData = "{ \"id\": 123, \"name\": \"Name goes here\" }".data(using: .utf8)!
        let decoder = JSONDecoder()
        
        let parsed: NamedGrouping = try! decoder.decode(NamedGrouping.self, from: jsonData)
        
        XCTAssertEqual(parsed, NamedGrouping(id: 123, name: "Name goes here"))
    }
}
