//
//  MoviesQueryResultParsingTests.swift
//  ms-tech-testTests
//
//  Created by acb on 11/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import XCTest
@testable import ms_tech_test

class MoviesQueryResultParsingTests: XCTestCase {
    
    
    func testParseMoviesQueryResult() {
        let jsonData = "{ \"results\": [ { \"vote_count\": 2868, \"id\": 181808, \"video\": false, \"vote_average\": 7.2, \"title\": \"Star Wars: The Last Jedi\", \"popularity\": 538.442545, \"poster_path\": \"/xGWVjewoXnJhvxKW619cMzppJDQ.jpg\", \"original_language\": \"en\", \"original_title\": \"Star Wars: The Last Jedi\", \"backdrop_path\": \"/5Iw7zQTHVRBOYpA0V6z0yypOPZh.jpg\", \"adult\": false, \"overview\": \"Rey develops her newly discovered abilities with the guidance of Luke Skywalker, who is unsettled by the strength of her powers. Meanwhile, the Resistance prepares to do battle with the First Order.\", \"release_date\": \"2017-12-13\" }, { \"vote_count\": 465, \"id\": 316029, \"video\": false, \"vote_average\": 8, \"title\": \"The Greatest Showman\", \"popularity\": 288.058167, \"poster_path\": \"/dfhztJRiElqmYW4kpvjYe1gENsD.jpg\", \"original_language\": \"en\", \"original_title\": \"The Greatest Showman\", \"backdrop_path\": \"/zpq404Sk7qQ7N4x3xOeNgp74GtU.jpg\", \"adult\": false, \"overview\": \"The story of American showman P.T. Barnum, founder of the circus that became the famous traveling Ringling Bros. and Barnum & Bailey Circus.\", \"release_date\": \"2017-12-20\" }, { \"vote_count\": 205, \"id\": 399055, \"video\": false, \"vote_average\": 6.9, \"title\": \"The Shape of Water\", \"popularity\": 285.380012, \"poster_path\": \"/iLYLADGA5oKGM92Ns1j9CDgk3iI.jpg\", \"original_language\": \"en\", \"original_title\": \"The Shape of Water\", \"backdrop_path\": \"/bAISaVhsaoyyQMZ55cpTwCdzek4.jpg\", \"adult\": false, \"overview\": \"An other-worldly story, set against the backdrop of Cold War era America circa 1962, where a mute janitor working at a lab falls in love with an amphibious man being held captive there and devises a plan to help him escape.\", \"release_date\": \"2017-12-01\" }, { \"vote_count\": 1179, \"id\": 354912, \"video\": false, \"vote_average\": 7.6, \"title\": \"Coco\", \"popularity\": 258.27342, \"poster_path\": \"/eKi8dIrr8voobbaGzDpe8w0PVbC.jpg\", \"original_language\": \"en\", \"original_title\": \"Coco\", \"backdrop_path\": \"/askg3SMvhqEl4OL52YuvdtY40Yb.jpg\", \"adult\": false, \"overview\": \"Despite his family’s baffling generations-old ban on music, Miguel dreams of becoming an accomplished musician like his idol, Ernesto de la Cruz. Desperate to prove his talent, Miguel finds himself in the stunning and colorful Land of the Dead following a mysterious chain of events. Along the way, he meets charming trickster Hector, and together, they set off on an extraordinary journey to unlock the real story behind Miguel's family history.\", \"release_date\": \"2017-10-27\" }, { \"vote_count\": 201, \"id\": 371638, \"video\": false, \"vote_average\": 7.4, \"title\": \"The Disaster Artist\", \"popularity\": 156.863916, \"poster_path\": \"/uCH6FOFsDW6pfvbbmIIswuvuNtM.jpg\", \"original_language\": \"en\", \"original_title\": \"The Disaster Artist\", \"backdrop_path\": \"/bAI7aPHQcvSZXvt7L11kMJdS0Gm.jpg\", \"adult\": false, \"overview\": \"An aspiring actor in Hollywood meets an enigmatic stranger by the name of Tommy Wiseau, the meeting leads the actor down a path nobody could have predicted; creating the worst movie ever made.\", \"release_date\": \"2017-11-30\" } ], \"page\": 1, \"total_results\": 873, \"dates\": { \"maximum\": \"2018-01-09\", \"minimum\": \"2017-11-21\" }, \"total_pages\": 44 }".data(using: .utf8)!
        
        let decoder = JSONDecoder()
        
        let parsed: MoviesQueryResult = try! decoder.decode(MoviesQueryResult.self, from: jsonData)
        XCTAssertEqual(parsed.results.count, 5)
        XCTAssertEqual(parsed.results.map { $0.title }, ["Star Wars: The Last Jedi", "The Greatest Showman", "The Shape of Water", "Coco", "The Disaster Artist"])
    }
    
}
