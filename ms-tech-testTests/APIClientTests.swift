//
//  APIClientTests.swift
//  ms-tech-testTests
//
//  Created by acb on 11/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import XCTest
import Hippolyte
@testable import ms_tech_test

class APIClientTests: XCTestCase {
    
    func testGetMovieDetails() {
        let apiURL = URL(string: "https://api.themoviedb.org/3/movie/123?api_key=abcdef1234")!
        var stub = StubRequest(method: .GET, url: apiURL)
        var response = StubResponse()
        response.body = "{ \"adult\": false, \"id\": 123, \"overview\": \"Plot goes here\", \"popularity\": 123.45, \"release_date\": \"2018-01-12\", \"runtime\": 120, \"title\": \"Dummy Data Movie\",  \"vote_average\": 2.3, \"vote_count\": 200 }".data(using: .utf8)
        stub.response = response
        Hippolyte.shared.add(stubbedRequest: stub)
        Hippolyte.shared.start()
        
        let expect = expectation(description: "getDetails(forMovie:)")
        let api = APIClient(key: "abcdef1234")
        api.getDetails(forMovieWithID: 123).onSuccess { (result) in
            XCTAssertEqual(result.id, 123)
            XCTAssertEqual(result.title, "Dummy Data Movie")
            expect.fulfill()

        }
        wait(for: [expect], timeout: 1.0)
    }
    
    func testGetCollectionDetails() {
        let apiURL = URL(string: "https://api.themoviedb.org/3/collection/99456?api_key=abcdef1234")!
        var stub = StubRequest(method: .GET, url: apiURL)
        var response = StubResponse()
        response.body = "{\"id\": 99456, \"name\": \"A Made-Up Collection\", \"overview\": \"Some more dummy data\", \"parts\": [ { \"id\": 999901,  \"title\": \"Made-Up Movie 1\", \"overview\": \"asdf\", } ]}".data(using: .utf8)
        stub.response = response
        Hippolyte.shared.add(stubbedRequest: stub)
        Hippolyte.shared.start()

        let expect = expectation(description: "getDetails(forCollection:)")
        let api = APIClient(key: "abcdef1234")
        api.getDetails(forCollectionWithID: 99456).onSuccess { (result) in
            XCTAssertEqual(result.id, 99456)
            XCTAssertEqual(result.name, "A Made-Up Collection")
            XCTAssertEqual(result.parts.count, 1)
            XCTAssertEqual(result.parts[0].id, 999901)
            expect.fulfill()
        }
        wait(for: [expect], timeout: 1.0)
    }

    func testGetNowPlaying() {
        let apiURL = URL(string: "https://api.themoviedb.org/3/movie/now_playing?api_key=abcdef1234")!
        var stub = StubRequest(method: .GET, url: apiURL)
        var response = StubResponse()
        response.body = "{\"results\": [ { \"id\": 999902,  \"title\": \"Some Dummy Data\", \"overview\": \"asdf\" }, { \"id\": 999903,  \"title\": \"More Dummy Data\", \"overview\": \"qwerty\", }]}".data(using: .utf8)
        stub.response = response
        Hippolyte.shared.add(stubbedRequest: stub)
        Hippolyte.shared.start()

        let expect = expectation(description: "getNowPlaying()")
        let api = APIClient(key: "abcdef1234")
        api.getNowPlaying().onSuccess { (result) in
            XCTAssertEqual(result.results.count, 2)
            XCTAssertEqual(result.results[0].id, 999902)
            XCTAssertEqual(result.results[1].id, 999903)
            XCTAssertEqual(result.results[0].title, "Some Dummy Data")
            XCTAssertEqual(result.results[1].title, "More Dummy Data")
            expect.fulfill()
        }
        wait(for: [expect], timeout: 1.0)
    }
}
