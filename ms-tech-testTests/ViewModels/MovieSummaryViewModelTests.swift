//
//  MovieSummaryViewModelTests.swift
//  ms-tech-testTests
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import XCTest
@testable import ms_tech_test

class MovieSummaryViewModelTests: XCTestCase {
    
    func testFill() {
        
        let summary = MovieSummary(id: 123, title: "Some movie", overview: "-")
        let viewModel = MovieSummaryViewModel(movieSummary: summary)
        XCTAssertEqual(viewModel.id, 123)
        XCTAssertEqual(viewModel.titleText, "Some movie")
    }
    
}
