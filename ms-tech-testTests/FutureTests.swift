//
//  FutureTests.swift
//  ms-tech-testTests
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import XCTest
@testable import ms_tech_test

class FutureTests: XCTestCase {
    
    class TestError: Error {
        var msg: String? = nil
        init(msg: String) { self.msg = msg }
    }
    
    func testAsyncFuture() {
        let f1 = Future<Int> { () -> Int in
            sleep(1)
            return 2+3
        }
        
        var result: Int? = nil
        f1.onSuccess { (v) in
            result = v
        }
        
        sleep(2)
        XCTAssertEqual(result, 5)
    }
    
    func testFutureCatchError() {
        let f1 = Future<Int>{
            throw TestError(msg:":-P")
        }
        var result: Int? = nil
        var error: Error? = nil
        f1.onSuccess { (v) in  result = v }
        f1.onFailure { (err) in error = err }
        sleep(1)
        XCTAssertNil(result)
        XCTAssertNotNil(error)
        XCTAssertEqual((error as? TestError)?.msg, ":-P")
    }
    
    func testMap() {
        let f1 = Future<Int> { () -> Int in
            sleep(1)
            return 2+3
        }
        
        let f2 = f1.map { $0 * 2 }
        
        var result: Int? = nil
        f2.onSuccess { (v) in
            result = v
        }
        
        sleep(2)
        XCTAssertEqual(result, 10)
    }
    
    func testMapThrow() {
        let f1 = Future<Int> { () -> Int in
            return 2+3
        }
        
        let f2 = f1.map { (Int) -> (Int) in throw TestError(msg:":-/") }
        
        var result: Int? = nil
        var error: Error? = nil
        f2.onSuccess { (v) in
            result = v
        }
        f2.onFailure { (err) in
            error = err
        }
        
        sleep(1)
        XCTAssertNil(result)
        XCTAssertEqual((error as? TestError)?.msg, ":-/")
    }
    
    func testFlatMap() {
        let f1 = Future<Int> { () -> Int in
            sleep(1)
            return 2+3
        }
        let f2 = f1.flatMap { (v) in Future { () -> Int in
            sleep(1)
            return v * 3
            }
        }
        
        var result: Int? = nil
        f2.onSuccess { (v) in
            result = v
        }
        
        sleep(3)
        XCTAssertEqual(result, 15)
    }
    
    func testFlatMapThrow1() {
        let f1 = Future<Int> { () -> Int in
            sleep(1)
            return 2+3
        }
        let f2 = f1.flatMap { (v) -> Future<Int> in
            throw TestError(msg: "/-:")
        }
        
        var result: Int? = nil
        var error: Error? = nil
        f2.onSuccess { (v) in
            result = v
        }
        f2.onFailure { (err) in
            error = err
        }
        
        sleep(2)
        XCTAssertNil(result)
        XCTAssertEqual((error as? TestError)?.msg, "/-:")
    }
    
    func testFlatMapThrow2() {
        let f1 = Future<Int> { () -> Int in
            return 2+3
        }
        let f2 = f1.flatMap { (v) in Future { () -> Int in
            throw TestError(msg: ":-0")
            }
        }
        
        var result: Int? = nil
        var error: Error? = nil
        f2.onSuccess { (v) in
            result = v
        }
        f2.onFailure { (err) in
            error = err
        }
        
        sleep(1)
        XCTAssertNil(result)
        XCTAssertEqual((error as? TestError)?.msg, ":-0")
    }
    
}
