//
//  MoviesQueryResult.swift
//  ms-tech-test
//
//  Created by acb on 11/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import Foundation

/** MoviesQueryResult is what we call what /movies/now-playing returns; a set of movies fitting a query that is not an identity of its own (like a genre or category) */

struct MoviesQueryResult: Decodable {
    let results: [MovieSummary]
    
    private enum CodingKeys: String, CodingKey {
        case results
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.results = try container.decode([MovieSummary].self, forKey: .results)
    }
}
