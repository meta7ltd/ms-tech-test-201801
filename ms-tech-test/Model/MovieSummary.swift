//
//  MovieSummary.swift
//  ms-tech-test
//
//  Created by acb on 11/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import Foundation

struct MovieSummary: Decodable {
    let id: Int
    let title: String
    let overview: String
    let posterPath: String?
    let backdropPath: String?
    let originalLanguage: String?
    let releaseDate: String?  // TODO: make this a Date?
    let popularity: Float?
    let votes: (average: Float, count: Int)?
    let isAdult: Bool?

    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case overview
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case releaseDate = "release_date"
        case popularity
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
        case isAdult = "adult"
        case originalLanguage = "original_language"
    }
    
    init(id: Int, title: String, overview: String, posterPath: String? = nil, backdropPath: String? = nil, originalLanguage: String? = nil, releaseDate: String? = nil, popularity: Float? = nil, votes: (Float, Int)? = nil, isAdult: Bool? = nil ) {
        self.id = id
        self.title = title
        self.overview = overview
        self.posterPath = posterPath
        self.backdropPath = backdropPath
        self.originalLanguage = originalLanguage
        self.releaseDate = releaseDate
        self.popularity = popularity
        self.votes = votes
        self.isAdult = isAdult
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
        self.overview = try container.decode(String.self, forKey: .overview)
        self.posterPath = try container.decodeIfPresent(String.self, forKey: .posterPath)
        self.backdropPath = try container.decodeIfPresent(String.self, forKey: .backdropPath)
        self.originalLanguage = try container.decodeIfPresent(String.self, forKey: .originalLanguage)
        self.releaseDate = try container.decodeIfPresent(String.self, forKey: .releaseDate)
        self.popularity = try container.decodeIfPresent(Float.self, forKey: .popularity)
        self.votes = try container.decodeIfPresent(Float.self, forKey: .voteAverage).flatMap { (avg) in
            try container.decodeIfPresent(Int.self, forKey: .voteCount).flatMap { (count) in
                (average: avg, count: count)
            }
        }
        self.isAdult = try container.decodeIfPresent(Bool.self, forKey: .isAdult)
    }
}
