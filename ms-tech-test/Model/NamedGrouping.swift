//
//  NamedGrouping.swift
//  ms-tech-test
//
//  Created by acb on 11/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import Foundation

/**
 A recurring pattern in the API: a grouping with an ID (local to its category), a name and possible artwork.
 We abstract this into one class to reuse it for items such as Collections and Genres
 */
struct NamedGrouping: Decodable {
    let id: Int
    let name: String
    let posterPath: String?
    let backdropPath: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.posterPath = try container.decodeIfPresent(String.self, forKey: .posterPath)
        self.backdropPath = try container.decodeIfPresent(String.self, forKey: .backdropPath)
    }

    init(id: Int, name: String, posterPath: String? = nil, backdropPath: String? = nil) {
        self.id = id
        self.name = name
        self.posterPath = posterPath
        self.backdropPath = backdropPath
    }
}

extension NamedGrouping: Equatable {
    static func ==(a: NamedGrouping, b: NamedGrouping) -> Bool {
        return a.id == b.id && a.name == b.name && a.posterPath == b.posterPath && a.backdropPath == b.backdropPath
    }
}
