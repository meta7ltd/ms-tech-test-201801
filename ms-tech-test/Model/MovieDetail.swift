//
//  MovieDetail.swift
//  ms-tech-test
//
//  Created by acb on 11/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import Foundation

struct MovieDetail: Decodable {
    
    let id: Int
    let title: String
    let overview: String
    let tagline: String?
    let posterPath: String?
    let backdropPath: String?
    let originalLanguage: String?
    let releaseDate: String?  // TODO: make this a Date?
    let popularity: Float?
    let votes: (average: Float, count: Int)?
    let budget: Int?
    let revenue: Int?
    let runtime: Measurement<UnitDuration>?
    let isAdult: Bool?
    let collection: NamedGrouping?
    let genres: [NamedGrouping]?

    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case overview
        case tagline
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case releaseDate = "release_date"
        case popularity
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
        case isAdult = "adult"
        case budget
        case revenue
        case runtime
        case originalLanguage = "original_language"
        case belongsToCollection = "belongs_to_collection"
        case genres
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
        self.overview = try container.decode(String.self, forKey: .overview)
        self.tagline = try container.decodeIfPresent(String.self, forKey: .tagline)
        self.posterPath = try container.decodeIfPresent(String.self, forKey: .posterPath)
        self.backdropPath = try container.decodeIfPresent(String.self, forKey: .backdropPath)
        self.originalLanguage = try container.decodeIfPresent(String.self, forKey: .originalLanguage)
        self.releaseDate = try container.decodeIfPresent(String.self, forKey: .releaseDate)
        self.popularity = try container.decodeIfPresent(Float.self, forKey: .popularity)
        self.runtime = try container.decodeIfPresent(Double.self, forKey: .runtime).map { Measurement(value: $0, unit: .minutes) }
        self.budget = try container.decodeIfPresent(Int.self, forKey: .budget)
        self.revenue = try container.decodeIfPresent(Int.self, forKey: .revenue)
        self.votes = try container.decodeIfPresent(Float.self, forKey: .voteAverage).flatMap { (avg) in
            try container.decodeIfPresent(Int.self, forKey: .voteCount).flatMap { (count) in
                (average: avg, count: count)
            }
        }
        self.isAdult = try container.decodeIfPresent(Bool.self, forKey: .isAdult)
        self.collection = try container.decodeIfPresent(NamedGrouping.self, forKey: .belongsToCollection)
        self.genres = try container.decodeIfPresent([NamedGrouping].self, forKey: .genres)
    }

}
