//
//  APIService.swift
//  ms-tech-test
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//
//  The global application service; this owns the app's API and all interactions with the API go through it

import Foundation

class APIService {
    // We make this a singleton for simplicity; in a more complex app, it would be better to have only one singleton, which would be an assembly that constructs and owns other app-level objects
    
    public static var sharedInstance = APIService()
    
    private let apiClient: APIClient
    
    private init() {
        guard let apiKey = Bundle.main.object(forInfoDictionaryKey: "MovieDBAPIKey") as? String else {
            fatalError("A MovieDBAPIKey entry must be specified in Info.plist")
        }
        self.apiClient = APIClient(key: apiKey)
    }
    
    //MARK: API methods passed through here
    func getNowPlaying() -> Future<MoviesQueryResult> {
        return self.apiClient.getNowPlaying()
    }
    
    func getDetails(forMovieWithID id: Int) -> Future<MovieDetail> {
        return self.apiClient.getDetails(forMovieWithID:id)
    }
    
    func getDetails(forCollectionWithID id: Int) -> Future<MovieCollection> {
        return self.apiClient.getDetails(forCollectionWithID:id)
    }

}
