//
//  MovieDetailViewModel.swift
//  ms-tech-test
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import UIKit

struct MovieDetailViewModel {
    let id: Int
    let titleText: String
    let overviewText: String
    let dateText: String?
    let votesText: String?
    let backdropImage: Future<UIImage>?
    let posterImage: Future<UIImage>?
    let collectionName: String?
    let collectionID: Int?
    let collectionImage: Future<UIImage>?

    
    init(movieDetail: MovieDetail) {
        // TODO: move this somewhere central, or fetch it from the configuration endpoint as specified and choose the width intelligently. Alas, there's not enough time for the latter, so this will have to suffice.
        let imageURLPrefix = "https://image.tmdb.org/t/p/w780"
        let backdropURL = movieDetail.backdropPath.flatMap { URL(string:imageURLPrefix+$0) }
        let posterURL = movieDetail.posterPath.flatMap { URL(string:imageURLPrefix+$0) }
        self.id = movieDetail.id
        self.titleText = movieDetail.title
        self.overviewText = movieDetail.overview
        self.dateText = movieDetail.releaseDate
        self.votesText = movieDetail.votes.map { (avg, count) in "\(avg*10)% (\(count) votes)" }
        self.backdropImage = backdropURL.map { UIImage.future(from: $0) }
        self.posterImage = posterURL.map { UIImage.future(from: $0) }
        self.collectionID = movieDetail.collection?.id
        self.collectionName = movieDetail.collection?.name
        self.collectionImage = movieDetail.collection?.backdropPath.flatMap { URL(string:imageURLPrefix+$0) }.map { UIImage.future(from:$0) }
    }
}
