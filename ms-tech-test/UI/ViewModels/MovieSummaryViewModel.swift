//
//  MovieSummaryViewModel.swift
//  ms-tech-test
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import UIKit

struct MovieSummaryViewModel {
    let id: Int
    let titleText: String
    let titleImage: Future<UIImage>?
    
    
    init(movieSummary: MovieSummary) {
        // TODO: move this somewhere central, or fetch it from the configuration endpoint as specified and choose the width intelligently. Alas, there's not enough time for the latter, so this will have to suffice.
        let imageURLPrefix = "https://image.tmdb.org/t/p/w500"
        let titleImageURL = movieSummary.posterPath.flatMap { URL(string:imageURLPrefix+$0) }
        self.id = movieSummary.id
        self.titleText = movieSummary.title
        self.titleImage = titleImageURL.map { UIImage.future(from: $0) }
    }
}
