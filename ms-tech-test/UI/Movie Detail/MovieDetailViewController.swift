//
//  MovieDetailViewController.swift
//  ms-tech-test
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

    var movieID: Int?
    var movieInfo: MovieDetailViewModel? {
        didSet {
            self.cellSequence = [.title, .overview] + ( movieInfo?.collectionID != nil ? [.collectionsHeading, .collectionInfo] : [])
            DispatchQueue.main.async {
                self.navigationItem.title = self.movieInfo?.titleText
                self.collectionView.reloadData()
            }

        }
    }

    /** We use a number of standard cells to compose a page, though they may not all appear for each page */
    enum CellType : String {
        case title = "MovieDetailTitleCell"
        case overview = "MovieDetailOverviewCell"
        case collectionsHeading = "MovieDetailCollectionsHeadingCell"
        case collectionInfo = "MovieDetailCollectionInfoCell"
    }
    
    // The sequence of cells to display for this movie's details
    var cellSequence: [CellType]?
    
    // We keep a captive copy of each type of cell, for purposes of measurement
    var prototypeCells: [CellType:UICollectionViewCell] = [:]


    override func viewDidLoad() {
        super.viewDidLoad()
        
        for cell in [CellType]([.title, .overview, .collectionsHeading, .collectionInfo]) {
            self.collectionView.register(UINib(nibName: cell.rawValue, bundle: nil), forCellWithReuseIdentifier: cell.rawValue)
            self.prototypeCells[cell] = Bundle.main.loadNibNamed(cell.rawValue, owner: nil, options: nil)![0] as! UICollectionViewCell
        }

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        if let id = self.movieID {
            APIService.sharedInstance.getDetails(forMovieWithID: id).onSuccess {
                self.movieInfo = MovieDetailViewModel(movieDetail: $0)
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                }
            }
        } else {
            self.movieInfo = nil
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MovieDetailViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int { return 1 }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cellSequence?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard
            let movieInfo = self.movieInfo,
            let cellSequence = self.cellSequence
        else { fatalError("This should never be reached") }
        let cellSpec = cellSequence[indexPath.row].rawValue
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellSpec, for: indexPath)
        (cell as? MovieDetailCellProtocol)?.fill(from: movieInfo)
        return cell
    }

}

extension MovieDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard
            let movieInfo = self.movieInfo,
            let cellType = self.cellSequence?[indexPath.row],
            let prototypeCell = self.prototypeCells[cellType]
        else { return .zero }
        // Measure the required size using the prototype cell.
        (prototypeCell as? MovieDetailCellProtocol)?.fill(from: movieInfo)
        
        let sizeToFit = CGSize(width: floor(self.collectionView.frame.size.width), height:0)
        return prototypeCell.subviews[0].systemLayoutSizeFitting(sizeToFit, withHorizontalFittingPriority: .required, verticalFittingPriority: .defaultLow)
    }

}
