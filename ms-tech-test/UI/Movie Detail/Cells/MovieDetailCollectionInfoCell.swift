//
//  MovieDetailCollectionInfoCell.swift
//  ms-tech-test
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import UIKit

class MovieDetailCollectionInfoCell: UICollectionViewCell {
    
    var id: Int?
    @IBOutlet var backdropImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!

    static let reuseIdentifier = "MovieDetailCollectionInfoCell"
}

extension MovieDetailCollectionInfoCell: MovieDetailCellProtocol {
    func fill(from detail: MovieDetailViewModel) {
        self.id = detail.id
        self.nameLabel.text = detail.collectionName
        detail.collectionImage?.onSuccess { (image) in
            DispatchQueue.main.async {
                if self.id == detail.id {
                    self.backdropImageView.image = image
                }
            }
        }
    }
}
