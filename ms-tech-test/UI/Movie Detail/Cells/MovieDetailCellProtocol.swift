//
//  MovieDetailCellProtocol.swift
//  ms-tech-test
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import Foundation

/** The protocol all movie detail detail cells implement, allowing them to configure themselves on command. */
protocol MovieDetailCellProtocol {
    /** Fill in the cell's details from the Recipe provided */
    func fill(from detail: MovieDetailViewModel)
}
