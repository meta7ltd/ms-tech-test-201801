//
//  MovieDetailOverviewCell.swift
//  ms-tech-test
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import UIKit

class MovieDetailOverviewCell: UICollectionViewCell {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var votesLabel: UILabel!
    var movieID: Int?

    static let reuseIdentifier = "MovieDetailOverviewCell"
}

extension MovieDetailOverviewCell: MovieDetailCellProtocol {
    func fill(from detail: MovieDetailViewModel) {
        self.movieID = detail.id
        self.overviewLabel.text = detail.overviewText
        self.votesLabel.text = detail.votesText
        detail.posterImage?.onSuccess { (image) in
            DispatchQueue.main.async {
                if self.movieID == detail.id {
                    self.posterImageView.image = image
                }
            }
        }
    }
}
