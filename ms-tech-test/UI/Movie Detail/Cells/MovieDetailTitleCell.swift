//
//  MovieDetailTitleCellCollectionViewCell.swift
//  ms-tech-test
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import UIKit

class MovieDetailTitleCell: UICollectionViewCell {

    @IBOutlet weak var backdropImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    var movieID: Int?

    static let reuseIdentifier = "MovieDetailTitleCell"
}

extension MovieDetailTitleCell: MovieDetailCellProtocol {
    func fill(from detail: MovieDetailViewModel) {
        self.movieID = detail.id
        self.titleLabel.text = detail.titleText
        detail.backdropImage?.onSuccess { (image) in
            DispatchQueue.main.async {
                if self.movieID == detail.id {
                    self.backdropImageView.image = image
                }
            }
        }
    }
}
