//
//  NowPlayingCollectionViewController.swift
//  ms-tech-test
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import UIKit

private let reuseIdentifier = "NowPlayingCell"

class NowPlayingCollectionViewController: UICollectionViewController {
    
    var movies: [MovieSummaryViewModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        APIService.sharedInstance.getNowPlaying().onSuccess { (result) in
            self.movies = result.results.map { MovieSummaryViewModel(movieSummary: $0) }
            DispatchQueue.main.async { self.collectionView?.reloadData() }
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailVC = segue.destination as? MovieDetailViewController,
            let selectedIndex = self.collectionView?.indexPathsForSelectedItems?.first?.row {
            
            detailVC.movieID = self.movies?[selectedIndex].id
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movies?.count ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.reuseIdentifier, for: indexPath) as! MovieCollectionViewCell
    
        if let movie = self.movies?[indexPath.row] {
            cell.fill(from: movie)
        }
        return cell
    }
    
}

extension NowPlayingCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width * 0.5 - 4.0, height: self.view.frame.size.width * 0.75)
    }
    
}
