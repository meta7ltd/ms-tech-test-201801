//
//  MovieCollectionViewCell.swift
//  ms-tech-test
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier: String = "MovieCollectionViewCell"

    @IBOutlet var posterImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
    var movieID: Int? // for setting images
    
    func fill(from viewModel: MovieSummaryViewModel) {
        self.movieID = viewModel.id
        self.titleLabel.text = viewModel.titleText
        viewModel.titleImage?.onSuccess { (image) in
            // if this cell belongs to another movie, do nothing
            if self.movieID == viewModel.id {
                DispatchQueue.main.async { self.posterImageView.image = image }
            }
        }
    }
}
