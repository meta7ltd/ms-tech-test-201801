//
//  UIImage+Future.swift
//  ms-tech-test
//
//  Created by acb on 12/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import UIKit

extension UIImage {
    class func future(from url: URL) -> Future<UIImage> {
        return URLRequest(url:url).future().map { (data, response) in
            guard
                let data = data,
                let image = UIImage(data: data)
                else { throw APIClient.APIError.malformedResponse(details: "No valid image data for \(url)") }
            return image
        }
    }
}
