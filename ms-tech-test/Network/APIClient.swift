//
//  APIClient.swift
//  ms-tech-test
//
//  Created by acb on 11/01/2018.
//  Copyright © 2018 acb. All rights reserved.
//

import Foundation

class APIClient {
    /** The API base URL */
    let urlBase = "https://api.themoviedb.org/3/"
    /** The API key */
    var key: String
    
    var queue: DispatchQueue

    /** The entities the API can refer to. These are not API operations, as they may theoretically be used with a number of REST methods (GET, POST, DELETE, &c.) */
    enum Entity {
        case nowPlaying
        case movieDetails(Int)
        case collectionDetails(Int)
        
        /** Return the endpoint, with any identifiers filled in  */
        var asPathSegment: String {
            switch(self) {
            case .nowPlaying: return "movie/now_playing"
            case .movieDetails(let id): return "movie/\(id)"
            case .collectionDetails(let id): return "collection/\(id)"
            }
        }
    }
    
    /** Errors which may be thrown by the API */
    enum APIError: Error {
        case invalidURL
        case malformedResponse(details: String?)
    }
    
    init(key: String) {
        self.key = key
        self.queue = DispatchQueue(label: "name.acb.ms-tech-test.api", qos: .userInitiated)
    }

    private func makeQueryURL(forEntity entity: Entity, params: [String:CustomStringConvertible]? = nil) -> URL {
        // TODO: URL-quote all params; this is not necessary for this exercise, but would be if the passed parameters could be arbitrary strings.
        let paramString = params.map { ($0.map { "\($0.0)=\($0.1)" }.joined(separator: "&")) + "&" } ?? ""
        guard let result =  URL(string:"\(self.urlBase)\(entity.asPathSegment)?\(paramString)api_key=\(self.key)") else {
            fatalError("Somehow, the API generated a broken URL")
        }
        return result
    }
    
    private func getResult<T: Decodable>(from entity: Entity, params: [String:CustomStringConvertible]? = nil) -> Future<T> {
        let url = self.makeQueryURL(forEntity: entity, params: params)
        return URLRequest(url: url).future().map { (data, response) in
            guard let data = data else { throw APIError.malformedResponse(details: "No data returned for \(url); \(response)") }
            return try JSONDecoder().decode(T.self, from:data)
        }
    }
    
    //MARK: Actual user-facing API calls

    func getNowPlaying() -> Future<MoviesQueryResult> {
        return getResult(from: .nowPlaying)
    }
    
    func getDetails(forMovieWithID id: Int) -> Future<MovieDetail> {
        return getResult(from: .movieDetails(id))
    }
    
    func getDetails(forCollectionWithID id: Int) -> Future<MovieCollection> {
        return getResult(from: .collectionDetails(id))
    }
    
}
